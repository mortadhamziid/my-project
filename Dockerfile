#Choisir une image de base avec Python
FROM python:3.9  

#Définir le répertoire de travail dans le conteneur
WORKDIR /app  

#Copier les fichiers de l’application dans le conteneur
COPY . /app  

#Installer les dépendances à partir du fichier requirements.txt
RUN pip install -r requirements.txt  

#Exposer le port sur lequel l’application écoutera
EXPOSE 5000  

#Définir la commande pour exécuter l’application
CMD ["python", "app.py"]  
